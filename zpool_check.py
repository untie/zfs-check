#!/usr/bin/env python

# TODO:
# * functionize all the things
# * use __main__ function
# * better integration of functions
# * generic email setup with parameters
# * mailing aggregation with attachments?
# * better understanding of smartctl commands

import os
import sys
import smtplib
import re
from glob import glob

# =====================================
# CONFIG

TEST = True

# Gmail setup (for simple usage)
email_addr = ''
email_pass = ''


def gmail(content):
    try:
        session = smtplib.SMTP('smtp.gmail.com', 587)
        session.ehlo()
        session.starttls()
        session.login(email_addr, email_pass)
        headers = "\r\n".join(["from: " + email_addr,
                               "subject: Disk Checking Script",
                               "to: " + email_addr,
                               "mime-version: 1.0",
                               "content-type: text/html"])
        session.sendmail(email_addr, email_addr, content)
    except Exception, e:
        print e
        sys.exit(1)


# execute a command and return its output
def cmd(c):
    try:
        proc = os.popen(c)
        out = proc.read().strip()
        return out
    except Exception, e:
        print e
        sys.exit(1)


# create a summary-text of failed-command's output and additional details
def summary(failed, details):
    s = failed
    s += "\n----------\n\n"
    s += details
    return s


# =====================================
# START

# ZPool checking
zpoolStatusX = cmd("/sbin/zpool status -x")

if TEST or zpoolStatusX.find("all pools are healthy") or zpoolStatusX.find("scrub repaired 0") == -1:
    zpoolStatus = cmd("/sbin/zpool status")
    txt = summary(zpoolStatusX, zpoolStatus)
    print TEST
    print zpoolStatusX.find("all pools are healthy")
    # gmail(txt)

disks = []
ssds = []

# seperating ssd's so I can check wear leveling later
for disk in glob("/dev/disk/by-id/ata*[0-9]?"):
    if "part" not in disk and "SSD" not in disk:
        disks.append(disk)
    else:
        disks.append(disk)
        ssds.append(disk)

# S.M.A.R.T. hdd check
for disk in disks:
    smartHealth = cmd('/usr/sbin/smartctl --health -d scsi ' + disk)
    if TEST or smartHealth.find("SMART Health Status: OK") == -1:
        smart = cmd('/usr/sbin/smartctl --all -d sat,12 ' + disk)
        txt = summary(smartHealth, smart)
        gmail("[NAS] S.M.A.R.T: " + disk + "\n\r" + txt)

# S.M.A.R.T. SDD wear_level check (should be added to hdd check since we're doing it anwaysy
wearCount = 100
regex = re.compile('.*Wear_Level+ing_Count\s+0x00\d+\s+(\d+).*')
for ssd in ssds:
    smartctl_cmd = '/usr/sbin/smartctl -a ' + ssd
    wearCheck = cmd(smartctl_cmd)
    r = regex.search(str(wearCheck))
    if r:
        wearCount = int(r.group(1))
    if wearCount <= 15:
        ssdtxt = "Warning ssd:" + ssd + \
            " has a low Wear_Level (" + str(
                wearCount) + "), it should be replaced soon"
        gmail(ssdtxt)

sys.exit(0)
